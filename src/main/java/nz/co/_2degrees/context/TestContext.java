package nz.co._2degrees.context;

import nz.co._2degrees.managers.PagesManager;
import nz.co._2degrees.managers.WebDriverManager;

public class TestContext {
    private WebDriverManager driverManager;
    private PagesManager pagesManager;
    private ScenarioContext scenarioContext;

    public TestContext() {
        driverManager = new WebDriverManager();
        scenarioContext = new ScenarioContext();
        pagesManager = new PagesManager(driverManager.getDriver());
    }

    public WebDriverManager getDriverManager() {
        return driverManager;
    }

    public PagesManager getPagesManager() {
        return pagesManager;
    }

    public ScenarioContext getScenarioContext() {
        return scenarioContext;
    }

}
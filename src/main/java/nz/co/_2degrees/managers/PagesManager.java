package nz.co._2degrees.managers;

import nz.co._2degrees.pages.HomePage;
import nz.co._2degrees.pages.PhonePage;
import nz.co._2degrees.pages.ShopPage;
import org.openqa.selenium.WebDriver;

public class PagesManager {

    private WebDriver driver;

    private HomePage homePage;
    private ShopPage shopPage;
    private PhonePage phonePage;

    public PagesManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        return (homePage == null) ? homePage = new HomePage(driver) : homePage;
    }

    public ShopPage getShopPage() {
        return (shopPage == null) ? shopPage = new ShopPage(driver) : shopPage;
    }

    public PhonePage getPhonePage() {
        return (phonePage == null) ? phonePage = new PhonePage(driver) : phonePage;
    }
}

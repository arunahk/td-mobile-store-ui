package nz.co._2degrees.managers;

import nz.co._2degrees.enums.DriverType;
import nz.co._2degrees.enums.EnvironmentType;
import nz.co._2degrees.util.ConfigFileReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.concurrent.TimeUnit;

public class WebDriverManager {

    private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
    private static final String FIREFOX_DRIVER_PROPERTY = "webdriver.gecko.driver";
    private static final String EDGE_DRIVER_PROPERTY = "webdriver.edge.driver";
    private static final String IE_DRIVER_PROPERTY = "webdriver.ie.driver";
    private static final String SAFARI_DRIVER_PROPERTY = "webdriver.safari.driver";

    private static DriverType driverType;
    private static EnvironmentType environmentType;
    private WebDriver driver;
    private ConfigFileReader configFileReader;

    public WebDriverManager() {
        configFileReader = FileReaderManager.getInstance().getConfigReader();
        driverType = configFileReader.getBrowser();
        environmentType = configFileReader.getEnvironment();
    }

    public WebDriver getDriver() {
        if (driver == null) {
            driver = createDriver();
        }
        return driver;
    }

    private WebDriver createDriver() {
        switch (environmentType) {
            case LOCAL:
                driver = createLocalDriver();
                break;
            case PRODUCTION:
                driver = createProductionDriver();
                break;
        }
        return driver;
    }

    private WebDriver createProductionDriver() {
        throw new RuntimeException("RemoteWebDriver is not yet implemented");
    }

    private WebDriver createLocalDriver() {
        switch (driverType) {
            case FIREFOX:
                System.setProperty(FIREFOX_DRIVER_PROPERTY, configFileReader.getDriverPath());
                driver = new FirefoxDriver();
                break;
            case CHROME:
                System.setProperty(CHROME_DRIVER_PROPERTY, configFileReader.getDriverPath());
                driver = new ChromeDriver();
                break;
            case INTERNETEXPLORER:
                System.setProperty(IE_DRIVER_PROPERTY, configFileReader.getDriverPath());
                driver = new InternetExplorerDriver();
                break;
            case EDGE:
                System.setProperty(EDGE_DRIVER_PROPERTY, configFileReader.getDriverPath());
                driver = new InternetExplorerDriver();
                break;
            case SAFARI:
                System.setProperty(SAFARI_DRIVER_PROPERTY, configFileReader.getDriverPath());
                driver = new InternetExplorerDriver();
                break;
        }

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(configFileReader.getImplicitlyWait(), TimeUnit.SECONDS);
        return driver;
    }

    public void quitDriver() {
        driver.quit();
    }

    public void closeDriver() {
        driver.close();
    }
}
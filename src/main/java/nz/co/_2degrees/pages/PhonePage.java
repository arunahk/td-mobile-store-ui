/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.co._2degrees.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author aruna.lakmal
 */
public class PhonePage extends BasePage {

    @FindBy(css = "main[id='app'] header h1")
    public WebElement hPhoneName;
    @FindBy(id = "phoneTotalMonthlyPrice")
    public WebElement inputPhoneTotalMonthlyPrice;
    @FindBy(css = "div main header h1")
    private WebElement hSubHeader;

    public PhonePage(WebDriver driver) {
        super(driver);
    }

    public float getPhonePrice() {
        return Float.parseFloat(inputPhoneTotalMonthlyPrice.getAttribute("value"));
    }

    public String getPageHeader() {
        return hSubHeader.getText();
    }
}

package nz.co._2degrees.pages;

import nz.co._2degrees.managers.FileReaderManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    private static final long TIMEOUT_SHORT = 10;
    private static final long TIMEOUT_MEDIUM = 30;
    private static final long TIMEOUT_LONG = 60;
    protected final String WEB_ROOT;
    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        WEB_ROOT = FileReaderManager.getInstance().getConfigReader().getApplicationUrl();
        PageFactory.initElements(this.driver, this);
    }

    protected void waitForElement(By selector) {
        (new WebDriverWait(driver, TIMEOUT_SHORT)).until(
                (new ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver d) {
                        return d.findElement(selector).getLocation() != null;
                    }
                }
                )
        );
    }

    protected void waitForPageLoad() {
        new WebDriverWait(driver, TIMEOUT_MEDIUM).until(
                ExpectedConditions.jsReturnsValue("return document.readyState == 'complete';")
        );
    }
}

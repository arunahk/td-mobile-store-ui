package nz.co._2degrees.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class ShopPage extends BasePage {

    @FindBy(css = "h1[class='itemlisting-heading']")
    private WebElement hSubHeader;

    @FindBy(css = "div[class='itemgrid'] a[class='item-container itembox']")
    private List<WebElement> phoneList;

    private WebElement selectPhone;

    public ShopPage(WebDriver driver) {
        super(driver);
    }

    public ArrayList<String> getPhoneList() {
        // grab the link of the page, phone name etc
        ArrayList<String> phones = new ArrayList<String>();
        for (WebElement webElement : phoneList) {
            WebElement nameElement = webElement.findElement(By.cssSelector("h3[class='item__model']"));
            phones.add(nameElement.getText().trim());
        }

        return phones;
    }

    public void selectPhone(String phone) {
        selectPhone = driver.findElement(By.cssSelector("h3[id='" + phone + "']")).findElement(By.xpath("../.."));
        selectPhone.click();
    }

    public String getPageHeader() {
        return hSubHeader.getText();
    }
}

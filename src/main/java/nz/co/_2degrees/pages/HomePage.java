package nz.co._2degrees.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(css = "a[title=Shop]")
    private WebElement lnkShop;

    public HomePage(WebDriver driver) {
        super(driver);
    }


    public void openHomePage() {
        driver.get(WEB_ROOT);
    }

    public void clickShop() {
        lnkShop.click();
    }
}

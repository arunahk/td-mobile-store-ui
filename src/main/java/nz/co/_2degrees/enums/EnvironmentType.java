package nz.co._2degrees.enums;

public enum EnvironmentType {
    LOCAL,
    STAGING,
    PRODUCTION
}
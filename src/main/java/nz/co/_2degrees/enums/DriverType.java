package nz.co._2degrees.enums;

public enum DriverType {
    FIREFOX,
    CHROME,
    EDGE,
    INTERNETEXPLORER,
    SAFARI
}

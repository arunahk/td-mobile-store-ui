Feature: Select a phone from 2Degrees online shop

  Background: Navigating to the online shop page
    Given I open 2Degrees web site
    When I click on link 'Shop'
    And navigate to the 'Shop' page
    Then I should see list of phones

  Scenario Outline: Check the price of a phone
    Given I click on the phone <phone>
    When I navigate to <phone> detail page
    Then I should see total monthly cost as <total>
    Examples: Single digits
      | phone        | total  |
      | iPhone XS    | 116.94 |
      | Galaxy Note9 | 111.38 |
      | Xperia XZ1   | 68.05  |

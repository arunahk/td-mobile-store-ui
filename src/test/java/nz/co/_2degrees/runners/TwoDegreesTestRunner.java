package nz.co._2degrees.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        plugin = {
                "pretty", "usage:target/reports-usage",
                "html:target/reports/html",
                "json:target/reports/cucumber.json",
                "junit:target/reports/cucumber.xml"
        },
        features = "src/test/resources/features",
        glue = "nz/co/_2degrees/steps"
)
public class TwoDegreesTestRunner extends AbstractTestNGCucumberTests {

    public TwoDegreesTestRunner() {
        super();
    }
}
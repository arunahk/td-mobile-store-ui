package nz.co._2degrees.steps;

import nz.co._2degrees.context.TestContext;

public abstract class BaseSteps {

    protected TestContext testContext;

    public BaseSteps(TestContext context) {
        testContext = context;
    }
}

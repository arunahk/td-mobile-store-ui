package nz.co._2degrees.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co._2degrees.context.TestContext;
import nz.co._2degrees.pages.PhonePage;
import org.testng.Assert;

public class PhonePageSteps extends BaseSteps {

    private PhonePage phonePage;

    public PhonePageSteps(TestContext context) {
        super(context);
        phonePage = testContext.getPagesManager().getPhonePage();
    }

    @When("I navigate to (.+) detail page")
    public void i_navigate_to_phone_detail_page(String phone) {
        Assert.assertTrue(phonePage.getPageHeader().contains(phone));
    }

    @Then("I should see total monthly cost as {float}")
    public void i_should_be_told(double price) {
        Assert.assertEquals(phonePage.getPhonePrice(), price, 2);
        // TODO: close after suite
        testContext.getDriverManager().closeDriver();
    }
}
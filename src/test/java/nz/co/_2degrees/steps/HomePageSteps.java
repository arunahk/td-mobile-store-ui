package nz.co._2degrees.steps;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import nz.co._2degrees.context.TestContext;
import nz.co._2degrees.pages.HomePage;

public class HomePageSteps extends BaseSteps {

    private HomePage homePage;

    public HomePageSteps(TestContext context) {
        super(context);
    }

    @Given("I open 2Degrees web site")
    public void i_open_2degrees_web_site() {
        homePage = testContext.getPagesManager().getHomePage();
        homePage.openHomePage();
    }

    @When("I click on link 'Shop'")
    public void i_click_on_link_shop() {
        homePage.clickShop();
    }
}
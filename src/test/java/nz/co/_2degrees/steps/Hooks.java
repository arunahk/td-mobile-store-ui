package nz.co._2degrees.steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import nz.co._2degrees.context.TestContext;

public class Hooks {

    private TestContext testContext;


    public Hooks(TestContext context) {
        testContext = context;
    }

    @Before
    public void BeforeSteps() {
		/*What all you can perform here
			Starting a webdriver
			Setting up DB connections
			Setting up test data
			Setting up browser cookies
			Navigating to certain page
			or anything before the test
		*/
        testContext.getDriverManager().getDriver().manage().deleteAllCookies();
    }

    @After
    public void AfterSteps() {

    }

}

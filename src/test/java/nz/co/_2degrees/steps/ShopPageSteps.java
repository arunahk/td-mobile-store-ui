package nz.co._2degrees.steps;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import nz.co._2degrees.context.TestContext;
import nz.co._2degrees.enums.Context;
import nz.co._2degrees.pages.ShopPage;
import org.testng.Assert;

public class ShopPageSteps extends BaseSteps {

    private ShopPage shopPage;

    public ShopPageSteps(TestContext context) {
        super(context);
        shopPage = testContext.getPagesManager().getShopPage();
    }

    @And("navigate to the 'Shop' page")
    public void navigate_to_the_shop_page() {
        Assert.assertEquals(shopPage.getPageHeader(), "Phones on 2degrees");
    }

    @Then("I should see list of phones")
    public void i_should_see_list_of_phones() {
        Assert.assertNotNull(shopPage.getPhoneList());
    }

    @Given("^I click on the phone (.+)$")
    public void i_click_on_the_phone(String phone) {
        // save selected phone for future use
        testContext.getScenarioContext().setContext(Context.PRODUCT_NAME, phone);
        shopPage.selectPhone(phone);
    }

}